﻿using System; // importiert die namespace
using System.Drawing;
using System.Windows.Forms;

namespace PingPong_v_1._0 // Wie ein Ordner und beinhaltet Klassen
{
    // Public = Damit wir überall in der Klasse Zugreifen können
    // Partial = definiert nur teilweise eine Klasse in der Methode
    // class = definiert eine Klasse
    // Geschweiftle kllamern definiert der Inhalt der Klasse
    // Frame 
    public partial class Form1 : Form // [system.Windows.Forms] sind in einer verschachtelte Namespace (.Forms= Klasse)
    {
        public Form1()// Form1 Konstruktur wird von Namespace intanziert
        {
            InitializeComponent();//generiert Die Graphische Darstellungen desProgramms This.etc
            _dx = 5;// Bewegung der Ball in :pixel 5 pixel rechts
            _dy = 2; // Bewegung der Ball in :pixel  pixel links
        }

        private void btnPlay_Click(object sender, EventArgs e)// Play button
        {
            tmrSpiel.Start();
        }
        int _dx = 5; // Initialisiert die Variable in Pinel x = Horizon y = vertikal
        int _dy = 2;
        private void tmrSpiel_Tick_1(object sender, EventArgs e) 
        {
            {
                int x = sqball.Location.X;
                int y = sqball.Location.Y;

                x += _dx;
                y += _dy;

                if (x < 0)// überprüfung mit if statement o punkt ist Oben linke Wand
                {
                    _dx = -_dx; //wenn der Ball in der Wand abprallt geht der ball in die andere Richtung Horizontal
                    x = -x;
                }

                if (x > gamepnl.Width - 25) // Horizontale bewegung rechte  Wand
                {
                    _dx = -_dx;
                    x = 2 * (gamepnl.Width - 25) - x;
                }

                if (y < 0) // Vertikale bewegung Obere Wand
                {
                    _dy = -_dy;
                    y = -y;
                }

                if (y > gamepnl.Height - 25) // Vertikale bewegung - Untere Wand
                {
                    _dy = -_dy;
                    y = 2 * (gamepnl.Height - 25) - y;
                }

                sqball.Location = new Point(x, y);// Point gehört als eine Punkt // Position der Ball wird neu gesetzt
           
            }
        }
    }
}

// Ereignis / event handling
// Event ist die Funktion selber 
// Btn_start_click = ist die funktion die abgerufen wird wenn ein Ereignis 