using System.Drawing;
using System.Windows.Forms;

namespace pingpong.v2
{
    public partial class PingPongForm : Form
    {
        private int _directionX = 5;
        private int _directionY = 2;
        private int _points = 0;
        public PingPongForm()
        {
            InitializeComponent();
        }

        private void StartGame(object sender, EventArgs e)
        {
            timer.Enabled = true;
            timer.Start();
        }

        private void EventHandler(object sender, EventArgs e)
        {
            // Bewegung des Balls
            ball.Location = new Point(ball.Location.X + _directionX, ball.Location.Y + _directionY);
            
            // Ball trifft auf rechten Spielfeldrand
            if (ball.Location.X >= gamePanel.Width - ball.Width-racketPicture.Width
                && ball.Location.Y + ball.Height >= racketPicture.Location.Y
                && ball.Location.Y <= racketPicture.Location.Y + racketPicture.Height)
            {
                _directionX = -_directionX;
                _points += 10; // Punktestand um 10 erhöhen
            }
            
            // Ball trifft auf linken Spielfeldrand
            if (ball.Location.X <= 0)
            {
                _directionX = -_directionX;
            }
            
            // Ball trifft auf oberen Spielfeldrand
            if (ball.Location.Y >= gamePanel.Height - ball.Height)
            {
                _directionY = -_directionY;
            }
            
            // Ball trifft auf rechten Spielfeldrand
            if (ball.Location.Y < 0)
            {
                _directionY = -_directionY;
            }
            
            // Punktestand anzeigen
            showPoints.Text = Convert.ToString(_points);
        }

        private void frmPingPong_Load(object sender, EventArgs e)
        {
            // Schläger ganz rechts ins Panel setzen
            racketPicture.Location = new Point(gamePanel.Width - racketPicture.Width, gamePanel.Height / 2);
            
            // Scrollbar rechts neben Panel setzen, Maximal und Minimalwert und aktuellen Wert einstellen
            racketScrollBar.Height = gamePanel.Height;
            racketScrollBar.Location = new Point(gamePanel.Location.X + gamePanel.Width, gamePanel.Location.Y);
            racketScrollBar.Minimum = 0;
            racketScrollBar.Maximum = gamePanel.Height - racketPicture.Height + racketScrollBar.LargeChange;
            racketScrollBar.Value = racketPicture.Location.Y;
        }

        private void vsbSchlägerRechts_Scroll(object sender, ScrollEventArgs e)
        {
            racketPicture.Location = new Point(racketPicture.Location.X, racketScrollBar.Value);
            racketScrollBar.Value = racketPicture.Location.Y;
        }
    }
}
