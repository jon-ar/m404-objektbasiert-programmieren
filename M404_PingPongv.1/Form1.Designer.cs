﻿namespace PingPong_v_1._0
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary> // eine Funktion wird ertsetz mit eine andere Funktion (Vererbung)
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) //ÜBERSCHREIBT oder ändert die Basis Klasse 
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);//Basis Klasse 
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tmrSpiel = new System.Windows.Forms.Timer(this.components);
            this.gamepnl = new System.Windows.Forms.PictureBox();
            this.sqball = new System.Windows.Forms.PictureBox();
            this.btnStart = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize) (this.gamepnl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.sqball)).BeginInit();
            this.SuspendLayout();
            // 
            // tmrSpiel
            // 
            this.tmrSpiel.Tick += new System.EventHandler(this.tmrSpiel_Tick_1);
            // 
            // gamepnl
            // 
            this.gamepnl.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.gamepnl.Location = new System.Drawing.Point(11, 11);
            this.gamepnl.Name = "gamepnl";
            this.gamepnl.Size = new System.Drawing.Size(600, 350);
            this.gamepnl.TabIndex = 0;
            this.gamepnl.TabStop = false;
            // 
            // sqball
            // 
            this.sqball.BackColor = System.Drawing.SystemColors.Highlight;
            this.sqball.Location = new System.Drawing.Point(41, 78);
            this.sqball.Name = "sqball";
            this.sqball.Size = new System.Drawing.Size(25, 25);
            this.sqball.TabIndex = 1;
            this.sqball.TabStop = false;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(82, 376);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(113, 32);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Spiel Starten";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(622, 413);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.sqball);
            this.Controls.Add(this.gamepnl);
            this.Location = new System.Drawing.Point(15, 15);
            this.Name = "Form1";
            this.Text = "PingPong v1";
            ((System.ComponentModel.ISupportInitialize) (this.gamepnl)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.sqball)).EndInit();
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.PictureBox gamepnl;
        private System.Windows.Forms.PictureBox sqball;
        private System.Windows.Forms.Timer tmrSpiel;

        #endregion
    }
}