﻿using System;
using System.Windows.Forms;

namespace Taschenrechner
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            double zahl1;
            double zahl2;
            int avg;
            int potenzwert;
        }

        private void btnAddition_Click(object sender, EventArgs e) //Addtition
        {
            var zahl1 = Convert.ToDouble(txtOperand1.Text);
            var zahl2 = Convert.ToDouble(txtOperand2.Text);
            var ergebnis = zahl1 + zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);
            lblOperator.Text = "+";
        }

        private void btnSubtraktion_Click(object sender, EventArgs e) //Subtraktion
        {
            var zahl1 = Convert.ToDouble(txtOperand1.Text);
            var zahl2 = Convert.ToDouble(txtOperand2.Text);
            var ergebnis = zahl1 - zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);
            lblOperator.Text = "-";
        }

        private void btnMittelwert_Click(object sender, EventArgs e) //Multiplikation
        {
            var zahl1 = Convert.ToDouble(txtOperand1.Text);
            var zahl2 = Convert.ToDouble(txtOperand2.Text);
            var ergebnis = zahl1 * zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);
            lblOperator.Text = "x";
        }

        private void btnDivision_Click(object sender, EventArgs e) //Division
        {
            var zahl1 = Convert.ToDouble(txtOperand1.Text);
            var zahl2 = Convert.ToDouble(txtOperand2.Text);
            var ergebnis = zahl1 / zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);
            lblOperator.Text = "/";
        }

        private void btnMittelwert_Click_1(object sender, EventArgs e) //Mittelwert
        {
            var zahl1 = Convert.ToDouble(txtOperand1.Text);
            var zahl2 = Convert.ToDouble(txtOperand2.Text);
            var ergebnis = (zahl1 + zahl2) / 2;
            lblErgebnis.Text = Convert.ToString(ergebnis);
            lblOperator.Text = "Avg";
        }

        private void btnMaximum_Click(object sender, EventArgs e) // Potenz
        {
            var zahl1 = Convert.ToDouble(txtOperand1.Text);
            var zahl2 = Convert.ToDouble(txtOperand2.Text); // Potenz Zahl mit ^ definiert funktioniert nicht wieso?
            var ergebnis = Math.Pow(zahl1, zahl2); // Math.pow ist eine Klasse
            lblErgebnis.Text = Convert.ToString(ergebnis);
            lblOperator.Text = "^";
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            var zahl1 = Convert.ToDouble(txtOperand1.Text);
            var zahl2 = Convert.ToDouble(txtOperand2.Text); // Potenz Zahl mit ^ definiert funktioniert nicht wieso?
            var ergebnis = zahl1 < zahl2 ? zahl2 : zahl1;
            lblErgebnis.Text = Convert.ToString(ergebnis);
            lblOperator.Text = "Max";
        }
    }
}
// if (zahl < zahl2)
// return zahl2;
// else
// return zahl1;