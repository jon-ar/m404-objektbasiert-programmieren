﻿namespace Schachbrett
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlchessboard = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.NumberofLines = new System.Windows.Forms.NumericUpDown();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NumberofLines)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlchessboard
            // 
            this.pnlchessboard.Location = new System.Drawing.Point(14, 82);
            this.pnlchessboard.Name = "pnlchessboard";
            this.pnlchessboard.Size = new System.Drawing.Size(305, 300);
            this.pnlchessboard.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.NumberofLines);
            this.panel2.Location = new System.Drawing.Point(14, 10);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(305, 66);
            this.panel2.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(59, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(229, 39);
            this.button1.TabIndex = 1;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // NumberofLines
            // 
            this.NumberofLines.Increment = new decimal(new int[] {2, 0, 0, 0});
            this.NumberofLines.Location = new System.Drawing.Point(14, 25);
            this.NumberofLines.Maximum = new decimal(new int[] {30, 0, 0, 0});
            this.NumberofLines.Name = "NumberofLines";
            this.NumberofLines.Size = new System.Drawing.Size(39, 20);
            this.NumberofLines.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 398);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlchessboard);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Schachbrett";
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.NumberofLines)).EndInit();
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown NumberofLines;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlchessboard;

        #endregion
    }
}