﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Schachbrett
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) //btnStart Eventhändler
        {
            Drawboard();
        }

        private void Drawboard()
        {
            {
                // Variable - DatenSatz
                int x; // Horizontale Achse
                int y; // Vertikal Achse
                int BoardNumber; // Anzahl Quadrat
                int width; // Breite
                int height; // höhe
                Label lbl; // Label ist eine Variabel lbl = window class
                pnlchessboard.Controls.Clear(); // Intanziert eine Sammlung der Kontrolle  Clear = keine Farbe
                BoardNumber = Convert.ToInt16(NumberofLines.Value);
                width = pnlchessboard.Width / BoardNumber; // Schachbrett geteilt durch den Anzahl der quadrat
                height = pnlchessboard.Height / BoardNumber;
                // Anzahl Quadrat rechnen ++ Auto increment(Addieren)
                for (y = 0; y < BoardNumber;
                    y++) // y gleich Null wenn y kleiner ist als Anzahl Quadrat dann 2 increment(Addieren) Vertikale Bereich
                for (x = 0; x < BoardNumber;
                    x++) // x gleich Null wenn x kleiner ist als Anzahl Quadrat dann 2 increment(Addieren) Horizontale Bereich
                    if ((x + y) % 2 == 0) // aus dem Internet eine mathematische lösung um die Anzahl Qudrat durch 2 zu teilen.
                    {
                        lbl = new Label(); // Spricht die angegebenen Variable Oben "Label" an
                        lbl.Location = new Point(x * height, y * height);
                        lbl.Size = new Size(width, height);
                        lbl.BackColor = Color.Black;
                        pnlchessboard.Controls.Add(lbl);
                    }
            }
        }
    }
}
// Label ist eine Variable innerhalb der Klasse 
// x + y % 2 == 0 erstellt 